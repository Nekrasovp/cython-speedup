cpdef int stern_n(int n):
    if n < 2:
        return n
    elif n % 2 == 0:
        return stern_n(n/2)
    else:
        return stern_n((n - 1)/2) + stern_n((n + 1)/2)

cpdef run_stern(int n):
    stern_array = list()
    for i in range(n):
        stern_array.append(stern_n(i))
    return stern_array