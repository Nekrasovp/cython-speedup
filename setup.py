from distutils.core import setup
from Cython.Build import cythonize

setup(name='Testing calculation',
      ext_modules=cythonize('*.pyx'),
      requires=['Cython'], )
