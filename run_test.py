import stern_python as python_s
import stern_cython as cython_s

import hofsconwc_python as python_h
import hofsconwc_cython as cython_h

import time

s_n = 10000

start = time.time()
result = python_s.run_stern(s_n)
end = time.time()
print(result[:20])
py_time = end - start
print(f'Python time = {py_time}')

start = time.time()
result = cython_s.run_stern(s_n)
end = time.time()
print(result[:20])
cy_time = end - start
print(f'Cython time = {cy_time}')

print(f'Speedup = {py_time / cy_time}')

print(40*'-')

s_n_2 = 1000000

start = time.time()
result = python_h.hofsconwc(s_n_2)
end = time.time()
print(list(result.values())[:20])
py_time = end - start
print(f'Python dict time = {py_time}')

start = time.time()
result = python_h.hofsconwc(s_n_2)
end = time.time()
print(result[:20])
py_time = end - start
print(f'Python list time = {py_time}')

start = time.time()
result = cython_h.hofsconwc(s_n_2)
end = time.time()
print(result[:20])
cy_time = end - start
print(f'Cython time = {cy_time}')
