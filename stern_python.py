def stern_n(n):
    if n < 2:
        return n
    elif n % 2 == 0:
        return stern_n(n/2)
    else:
        return stern_n((n - 1)/2) + stern_n((n + 1)/2)

def run_stern(n):
    stern_array = list()
    for i in range(n):
        stern_array.append(int(stern_n(i)))
    return stern_array
